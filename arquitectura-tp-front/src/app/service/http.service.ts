import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient
  ) { }


  public newDashboard(dashboard): Observable<any> {

    const url = 'http://ec2-18-117-178-234.us-east-2.compute.amazonaws.com:3001/dashboard';
    
    return this.httpClient.post(url, dashboard)
  }
}
