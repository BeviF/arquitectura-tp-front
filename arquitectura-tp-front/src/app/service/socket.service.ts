import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  constructor(private socket: Socket) { }


  public sendMessage(message) {
    
    if (this.socket.ioSocket.connected) {
      this.socket.emit('repo-commit', message);
      
      return true;
    } else {
      return false;
    }
  }

  public sendDashboard(id) {
    this.socket.emit('repo-clone', { id: id });
  }

  public sendAllReposInfo() {
    this.socket.emit('repos-info', { id: '' });
  }

  public checkSocketStatus() {
    
    return Observable.create((observer) => {


      this.socket.on('disconnect', function () {
        observer.next();
      });
    });

  }

  public onConnect() {
    
    return Observable.create((observer) => {


      this.socket.on('connect', function () {        
        observer.next();
      });
    });

  }

  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('repo-synchro', (message) => {
        observer.next(message);
      });

      // this.socket.on('repo-clone', (message) => {
      //   observer.next(message);
      // });
    });
  }

  public getDashboard = () => {
    return Observable.create((observer) => {
      this.socket.on('repo-clone', (message) => {
        observer.next(message);
      });
    });
  }

  public getAllReposInfo = () => {
    return Observable.create((observer) => {
      this.socket.on('repos-info', (message) => {        
        observer.next(message);
      });
    });
  }

  public restoreRepository = () => {
    return Observable.create((observer) => {
      this.socket.on('restore-repository', (message) => {
        observer.next(message);
      });
    });
  }





}
