import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDashboardModalComponent } from './new-dashboard-modal.component';

describe('NewDashboardModalComponent', () => {
  let component: NewDashboardModalComponent;
  let fixture: ComponentFixture<NewDashboardModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDashboardModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDashboardModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
