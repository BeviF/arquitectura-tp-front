import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-new-dashboard-modal',
  templateUrl: './new-dashboard-modal.component.html',
  styleUrls: ['./new-dashboard-modal.component.scss']
})
export class NewDashboardModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NewDashboardModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }


  ngOnInit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

}
