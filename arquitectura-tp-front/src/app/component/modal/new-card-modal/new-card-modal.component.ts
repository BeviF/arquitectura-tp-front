import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-new-card-modal',
  templateUrl: './new-card-modal.component.html',
  styleUrls: ['./new-card-modal.component.scss']
})
export class NewCardModalComponent implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<NewCardModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }


  ngOnInit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

}
