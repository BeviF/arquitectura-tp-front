import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isSubmitted: Boolean;
  isLoginError: Boolean;
  loginForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required]
    });
  }


  login() {
    if (!this.loginForm.valid) {
      return
    }
    this.isLoginError = false
    this.isSubmitted = true
    
    localStorage.setItem('username', this.loginForm.get('username').value);
    this.resetLoginForm()
    this.router.navigate(['./dashboard']);
  }


  private resetLoginForm() {
    this.isSubmitted = false;
    this.loginForm.reset();
  }

}
