import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { NewCardModalComponent } from '../modal/new-card-modal/new-card-modal.component';
import { NewDashboardModalComponent } from '../modal/new-dashboard-modal/new-dashboard-modal.component';
import { HttpService } from '../../service/http.service';


import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

const Automerge = require('automerge')

import { SocketService } from '../../service/socket.service';
const jsonpatch = require('fast-json-patch')

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  dashboard: any = [];
  modelTodoDashboard: any = [];
  modelInProgressDashboard: any = [];
  modelDoneDashboard: any = [];
  counter: number = 0;
  observer: any;
  dragEvent: any;
  card: any = {};
  username: string;
  newDashboard: any = {};
  repositories: any = [];
  repositoryId: number = 0;

  changes: any = [];
  offline: boolean = false;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private httpService: HttpService,
    private socketService: SocketService) {
    this.username = localStorage.getItem('username');
  }

  ngOnInit() {

    this.getDashboard(this.repositoryId);
    this.sendAllReposInfo();


    this.socketService
      .getMessages()
      .subscribe((changesToApply: any) => {

        const changesJson = JSON.parse(changesToApply);
        if (changesJson.id != this.repositoryId) {
          return;
        }
        try {
          this.dashboard.repository.cards = jsonpatch.applyPatch(this.dashboard.repository.cards, JSON.parse(changesToApply).changes, true, false).newDocument
          if (this.dragEvent) {
            this.dragEvent.source.reset()
            this.dragEvent = undefined;
          }
        }
        catch (patchException) {
          alert("Ocurrio un error al intentar operar, se reestablecio al último estado correcto.")
          this.getDashboard(this.repositoryId);
        }


        this.updateLists()

        this.observer = jsonpatch.observe(this.dashboard.repository.cards)

      });


    this.socketService
      .getDashboard()
      .subscribe((dashboard: any) => {

        this.dashboard = JSON.parse(dashboard);

        this.updateLists()

        this.observer = jsonpatch.observe(this.dashboard.repository.cards)

      });


    this.socketService
      .restoreRepository()
      .subscribe((dashboard: any) => {

        this.dashboard = JSON.parse(dashboard);

        this.updateLists()

        this.observer = jsonpatch.observe(this.dashboard.repository.cards)
        alert("Ocurrio un error al intentar operar, se reestablecio al último estado correcto.")
      });


    this.socketService
      .getAllReposInfo()
      .subscribe((repositories: any) => {
        this.repositories = JSON.parse(repositories);

      });


    this.socketService.checkSocketStatus()
      .subscribe(() => {
        this.offline = true;
        alert("Servidor caido")
      })


      this.socketService.onConnect()
      .subscribe(() => {
        if(this.offline == true){
          this.offline = false;

          if (this.socketService.sendMessage(JSON.stringify({ changes: this.changes, id: this.dashboard.id }))) {
            this.changes = []
          }
          
          alert("Sincronizando repo")
          this.getDashboard(this.repositoryId);
        }
      }) 

  }

  changeCardStatus(event: CdkDragDrop<string[]>, status) {
    let draggedCard = event.previousContainer.data[event.previousIndex] as any;

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      //Aca se reordenan los elementos de la misma lista
      (event.container.data as any[]).forEach((card, index) => card.index = index)
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      (event.container.data as any[]).forEach((card, index) => card.index = index);
      (event.previousContainer.data as any[]).forEach((card, index) => card.index = index);
    }

    let index = this.dashboard.repository.cards.findIndex(dashBoardCard => (dashBoardCard.index == draggedCard.index && dashBoardCard.title == draggedCard.title))
    
    this.dashboard.repository.cards[index].status = status;

    let changes = jsonpatch.generate(this.observer);

    this.changes = this.changes.concat(changes)

    if (this.socketService.sendMessage(JSON.stringify({ changes: this.changes, id: this.dashboard.id }))) {
      this.changes = []
      this.offline = false;
    }

  }

  getDashboard(id) {
    console.log("Solicitando Repo")
    this.socketService.sendDashboard(id);
  }


  sendAllReposInfo() {
    this.socketService.sendAllReposInfo();
  }


  deleteCard(element) {

    let index = this.dashboard.repository.cards.findIndex(dashBoardCard => ( dashBoardCard.index == element.index && dashBoardCard.title == element.title))

    this.dashboard.repository.cards[index].deleted = true;
    this.updateLists()

    let changes = jsonpatch.generate(this.observer);

    this.changes = this.changes.concat(changes)

    if (this.socketService.sendMessage(JSON.stringify({ changes: this.changes, id: this.dashboard.id }))) {
      this.changes = []
      this.offline = false;
    }
  }


  updateLists() {
    this.modelTodoDashboard = this.dashboard.repository.cards.filter(card => card.status == 'To-do').sort((a, b) => a.index - b.index);
    this.modelInProgressDashboard = this.dashboard.repository.cards.filter(card => card.status == 'In-Progress').sort((a, b) => a.index - b.index);
    this.modelDoneDashboard = this.dashboard.repository.cards.filter(card => card.status == 'Done').sort((a, b) => a.index - b.index);
  }

  dragStarted(event: CdkDragDrop<string[]>) {
    this.dragEvent = event;
  }

  dragEnded(event: CdkDragDrop<string[]>) {
    this.dragEvent = undefined;
  }



  openCardModal(mode: string, card: any) {

    let modalCard = JSON.parse(JSON.stringify(card));

    const dialogRef = this.dialog.open(NewCardModalComponent, {
      maxWidth: "90%",
      width: "90%",
      data: { card: modalCard, mode: mode }
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        result.metadata = {
          author: this.username,
          time: Date.now()
        }

        if (mode == 'new') {
          result.metadata = {
            author: this.username,
            time: Date.now()
          }
          result.status = "To-do";
          result.deleted = false;
          result.index = this.modelTodoDashboard.length;

          this.dashboard.repository.cards.push(result)
          this.updateLists()

          let changes = jsonpatch.generate(this.observer);

          this.changes = this.changes.concat(changes)

          if (this.socketService.sendMessage(JSON.stringify({ changes: this.changes, id: this.dashboard.id }))) {
            this.changes = []
            this.offline = false;
          }
        } else if (mode == 'edit') {

          result.metadata.time = Date.now();          

          let index = this.dashboard.repository.cards.findIndex(dashBoardCard => (dashBoardCard.index == modalCard.index && dashBoardCard.status == modalCard.status))
          this.dashboard.repository.cards[index] = result;
          this.updateLists()

          let changes = jsonpatch.generate(this.observer);
                    
          this.changes = this.changes.concat(changes)

          if (this.socketService.sendMessage(JSON.stringify({ changes: this.changes, id: this.dashboard.id }))) {
            this.changes = []
            this.offline = false;
          }

        }
      }
    });
  }


  logOut() {
    localStorage.removeItem('username');
    this.router.navigate(['./login']);

  }


  openDashboardModal(dashboard: any) {

    let modalDashboard = JSON.parse(JSON.stringify(dashboard));

    const dialogRef = this.dialog.open(NewDashboardModalComponent, {
      maxWidth: "90%",
      width: "90%",
      data: { dashboard: modalDashboard }
    });


    dialogRef.afterClosed().subscribe(result => {

      if (result) {

        result.author = this.username;

        this.httpService.newDashboard(result).subscribe(
          response => {

            this.newDashboard = {};

            this.repositories.push(response);
            this.sendAllReposInfo();

          },
          err => {
            this.newDashboard = {};
          });
      }
    });
  }

  selectRepository(repository) {
    this.repositoryId = repository.id;

    this.getDashboard(this.repositoryId);
  }
}


