import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { LoginGuard } from './middleware/login-guard';

const config: SocketIoConfig = { url: 'http://ec2-18-117-178-234.us-east-2.compute.amazonaws.com:3000', options: {} };

import { DragDropModule } from '@angular/cdk/drag-drop';

import {

  MatTableModule,
  MatIconModule,
  MatCardModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule

} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NewCardModalComponent } from './component/modal/new-card-modal/new-card-modal.component';
import { LoginComponent } from './component/login/login.component';
import { NewDashboardModalComponent } from './component/modal/new-dashboard-modal/new-dashboard-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NewCardModalComponent,
    LoginComponent,
    NewDashboardModalComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    DragDropModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SocketIoModule.forRoot(config)

  ],
  exports: [NewCardModalComponent, NewDashboardModalComponent],
  entryComponents: [NewCardModalComponent, NewDashboardModalComponent],
  providers: [
    LoginGuard,
    MatDialog,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }],
  bootstrap: [AppComponent]
})
export class AppModule { }
